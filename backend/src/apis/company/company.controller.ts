import { Body, Controller, Delete, Get, Post } from '@nestjs/common';

import { Company } from './company.entity';
import { User } from '../users/user.entity';

@Controller('companies')
export class CompanyController {

    @Get()
    findAll(): any {
        return Company.find({ relations: ["companies"] });
    }


    @Post()
    async create(@Body() payload) {
        const company = new Company();
        // just skip the validation for now 

        const { name, desc, userId } = payload; 

        company.name = name;
        company.desc = desc; 

        // well one company only belong to one user ... 
        company.userId = userId; 

        return company.save();
    }

    
    @Delete()
    remove(id: number) {
        return Company 
            .createQueryBuilder()
            .delete()
            .from(Company)
            .where("id = :id", { id })
            .execute();
    }


}
