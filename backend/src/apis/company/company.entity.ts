import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

// id, name, short description, and creation date

import { User } from '../users/user.entity'

@Entity('companies')
export class Company extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column()
  name: string;

  @Column()
  desc: string;

  @CreateDateColumn()
  createdAt: Date;


  @ManyToOne(() => User, user => user.companies)
  user: User;

}