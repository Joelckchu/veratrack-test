import { Body, Controller, Get, Post } from '@nestjs/common';
import { User } from './user.entity';

const EMAIL_EXIST_ERR_MSG = 'This email address already exists';

@Controller('users')
export class UserController {

  @Get()
  findAll(): any {
    return User.find();
  }

  @Post()
  async create(@Body() payload) {
    
    const email = payload.email;
    // check if the email already existed 
    const hasEmail = await User.findOne({ email });

    if (hasEmail) {
      // really should be throw `new Error(EMAIL_EXIST_ERR_MSG)` inside, but anyway
      return EMAIL_EXIST_ERR_MSG;
    }

    const user = new User();

    user.name = payload.name;
    user.email = payload.email;

    return user.save();
  }
}
